package br.com.spedison.permuta.sequences;

import java.util.List;

/**
 * File ConfigSequence.java created by @GrupoAlpha on 04/11/2015 at 22:35 for projetct Permuta_Library.
 */
public class ConfigSequence {

    private List<Long> startSequence;
    private int lengthSequence;
    private int numberOfElements;
    private long numberOfSteps;
    private long numberMaxOfSteps;
    private long firstElement;


    public List<Long> getStartSequence() {
        return startSequence;
    }

    public void setStartSequence(List<Long> startSequence) {
        this.startSequence = startSequence;
    }

    public int getLengthSequence() {
        return lengthSequence;
    }

    public void setLengthSequence(int lengthSequence) {
        this.lengthSequence = lengthSequence;
    }

    public int getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public long getNumberOfSteps() {
        return numberOfSteps;
    }

    public void setNumberOfSteps(long numberOfSteps) {
        this.numberOfSteps = numberOfSteps;
    }

    public long getNumberMaxOfSteps() {
        return numberMaxOfSteps;
    }

    public void setNumberMaxOfSteps(long numberMaxOfSteps) {
        this.numberMaxOfSteps = numberMaxOfSteps;
    }

    public long getFirstElement() {
        return firstElement;
    }

    public void setFirstElement(long firstElement) {
        this.firstElement = firstElement;
    }
}
