package br.com.spedison.permuta.sequences;

import br.com.spedison.permuta.interfaces.ReceiveGame;

import java.util.ArrayList;

/**
 * File ExecuteSequence.java created by @GrupoAlpha on 04/11/2015 at 22:42 for projetct Permuta_Library.
 */
public class ExecuteSequence {

    ReceiveGame receiveGame;
    ArrayList<Long> sequence;
    ConfigSequence configSequence;

    public ReceiveGame getReceiveGame() {
        return receiveGame;
    }

    public void setReceiveGame(ReceiveGame receiveGame) {
        this.receiveGame = receiveGame;
    }

    public ConfigSequence getConfigSequence() {
        return configSequence;
    }

    public void setConfigSequence(ConfigSequence configSequence) {
        this.configSequence = configSequence;
    }

    long nextElement;
    int currentPosition;
    int startPosition;


    public void execute() {
        currentPosition = configSequence.getStartSequence().size();
        startPosition = currentPosition;

        if (currentPosition == 0)
            nextElement = configSequence.getFirstElement();
        else
            nextElement = configSequence.getStartSequence().get(currentPosition - 1) + 1;

        sequence = new ArrayList<>(configSequence.getLengthSequence());
        for (int x = 0; x < configSequence.getLengthSequence(); x++)
            sequence.add(0L);

        int pos = 0;
        for (Long i : configSequence.getStartSequence()) {
            sequence.set(pos++, i);
        }

        execute(currentPosition, nextElement);
    }


    public void execute(ReceiveGame receiveGame, ConfigSequence configSequence) {
        this.receiveGame = receiveGame;
        this.configSequence = configSequence;

        execute();
    }


    private void execute(int currentPosition, long currentElement) {


        long lastElement = configSequence.getFirstElement() + configSequence.getNumberOfElements();
        lastElement -=  (configSequence.getLengthSequence() - currentPosition);
        lastElement ++;

        for (long x = currentElement;
             x < lastElement;
             x++) {

            sequence.set(currentPosition, x);

            if (currentPosition < (configSequence.getLengthSequence() - 1)) {
                currentPosition++;
                execute(currentPosition, x + 1);
                currentPosition--;
            } else {
                receiveGame.onReceiveGame(sequence);
            }
        }
    }

}
