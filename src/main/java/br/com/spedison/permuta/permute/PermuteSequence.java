package br.com.spedison.permuta.permute;

import br.com.spedison.permuta.interfaces.ReceiveGame;

import java.util.ArrayList;

/**
 * File PermuteSequence.java created by @GrupoAlpha on 04/11/2015 at 23:45 for projetct Permuta_Library.
 */
public class PermuteSequence {

    ReceiveGame receiveGame;

    public void setReceiveGame(ReceiveGame receiveGame) {
        this.receiveGame = receiveGame;
    }


    public void recursivePermute(ArrayList<Long> str) {
        recursivePermute(str, 0);
    }

    public void recursivePermute(ArrayList<Long> arr, int k) {

        int i, len;
        len = arr.size();

        if (k == len) {
            receiveGame.onReceiveGame(arr);
        } else {
            for (i = k; i < len; i++) {
                exchangeCharacters(arr, k, i);
                //receiveGame.onReceiveGame(arr);
                recursivePermute(arr, k + 1);
                exchangeCharacters(arr, i, k);
            }
        }
    }

    private void exchangeCharacters(ArrayList<Long> arr, int p1, int p2) {

        Long tmp;
        tmp = arr.get(p1);
        arr.set(p1, arr.get(p2));
        arr.set(p2, tmp);
    }


}
