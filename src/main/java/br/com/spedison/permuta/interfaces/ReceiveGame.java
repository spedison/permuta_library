package br.com.spedison.permuta.interfaces;

import java.util.ArrayList;

/**
 * File ReceiveGame.java created by @GrupoAlpha on 04/11/2015 at 22:34 for projetct Permuta_Library.
 */
public interface ReceiveGame {

    void onReceiveGame(ArrayList<Long> sequence);

}
