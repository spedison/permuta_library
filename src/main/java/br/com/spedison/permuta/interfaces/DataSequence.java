package br.com.spedison.permuta.interfaces;

/**
 * File DataSequence.java created by @GrupoAlpha on 04/11/2015 at 22:36 for projetct Permuta_Library.
 */
public interface DataSequence {
    Object getItem(long position);
    long   getPosition(Object data);
}
