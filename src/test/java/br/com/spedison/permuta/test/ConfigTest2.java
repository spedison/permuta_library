package br.com.spedison.permuta.test;

import br.com.spedison.permuta.sequences.ConfigSequence;

import java.util.ArrayList;

/**
 * File ConfigTest1.java created by @GrupoAlpha on 04/11/2015 at 22:51 for projetct Permuta_Library.
 */
public class ConfigTest2 extends ConfigSequence {

    public ConfigTest2() {

        setLengthSequence(4);
        setNumberMaxOfSteps(-1);
        setNumberOfElements(10);
        setNumberOfSteps(0);
        ArrayList<Long> startSequence = new ArrayList<>();
        startSequence.add(1L);
        startSequence.add(5L);
        setStartSequence(startSequence);
        setFirstElement(1);
    }
}
