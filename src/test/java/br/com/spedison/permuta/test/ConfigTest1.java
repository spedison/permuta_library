package br.com.spedison.permuta.test;

import br.com.spedison.permuta.sequences.ConfigSequence;

import java.util.ArrayList;

/**
 * File ConfigTest1.java created by @GrupoAlpha on 04/11/2015 at 22:51 for projetct Permuta_Library.
 */
public class ConfigTest1 extends ConfigSequence {

    public ConfigTest1() {

        setLengthSequence(4);
        setNumberMaxOfSteps(-1);
        setNumberOfElements(10);
        setNumberOfSteps(0);
        setStartSequence(new ArrayList<>());
        setFirstElement(1);
    }
}
