package br.com.spedison.permuta.test;

import br.com.spedison.permuta.interfaces.ReceiveGame;

import java.util.ArrayList;

/**
 * File PrintSequence.java created by @GrupoAlpha on 04/11/2015 at 23:15 for projetct Permuta_Library.
 */
public class PrintSequence  implements ReceiveGame{

    @Override
    public void onReceiveGame(ArrayList<Long> sequence) {
        sequence.stream().forEach(System.out::println);
        System.out.println("===============");
    }
}
