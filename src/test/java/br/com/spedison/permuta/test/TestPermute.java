package br.com.spedison.permuta.test;

import br.com.spedison.permuta.permute.PermuteSequence;
import org.junit.Test;

import java.util.ArrayList;

/**
 * File TestRun.java created by @GrupoAlpha on 04/11/2015 at 23:18 for projetct Permuta_Library.
 */
public class TestPermute {

    @Test
    public void run(){

        ArrayList<Long> a = new ArrayList<>();
        a.add(1L);
        a.add(2L);
        a.add(3L);
        a.add(4L);

        PermuteSequence p = new PermuteSequence();
        p.setReceiveGame(new PrintSequence());
        p.recursivePermute(a);
    }
}
