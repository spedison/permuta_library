package br.com.spedison.permuta.test;

import br.com.spedison.permuta.sequences.ExecuteSequence;
import org.junit.Test;

/**
 * File TestRun.java created by @GrupoAlpha on 04/11/2015 at 23:18 for projetct Permuta_Library.
 */
public class TestRun2 {

    @Test
    public void run(){

        ExecuteSequence executeSequence = new ExecuteSequence();
        executeSequence.setConfigSequence(new ConfigTest2());
        executeSequence.setReceiveGame(new PrintSequence());
        executeSequence.execute();
    }
}
